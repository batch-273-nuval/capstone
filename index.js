class Customer {

	constructor(email) {
		this.email = email;
		this.cart = new Cart();
		this.orders = [];
	}

	checkOut() {
    	if (this.cart.contents.length !== undefined && this.cart.contents.length !== 0) {
    		let cartItem = {};
    		cartItem.products = this.cart.contents;
    		cartItem.totalAmount = this.cart.totalAmount;
    		this.orders.push(cartItem);
    		this.cart.clearCartContents();
    	}
    	return this;
  }


}



class Cart {

	constructor() {
		this.contents = [];
		this.totalAmount = 0;
	}

	addToCart(product, quantity) {
		let cartItem = {};
    	cartItem.product = product;
    	cartItem.quantity = quantity;
      	this.contents.push(cartItem);
    	return this;
  	}


	showCartContents() {
		console.log(this.contents);
		return this;
	}

	updateProductQuantity(name, quantity){
        this.contents.find(content => {
        	content.product.name === name ? content.quantity = quantity : null
        })
        return this;
    }
	
	clearCartContents() {
		this.contents = [];
    	return this;
	}

	computeTotal() {
    let totalAmount = 0;
    this.contents.forEach((cartItem) => {
		totalAmount = (cartItem.product.price * cartItem.quantity);
    	});
    	this.totalAmount = totalAmount;
    	return this;
  }

}



class Product {

	constructor(name, price) {
		this.name = name;
		this.price = price;
		this.isActive = true;
	}

	archive() {
		if (this.isActive === true) {
			this.isActive = false;
			return this;
		}
	}

	updatePrice(newPrice) {
		this.price = newPrice;
		return this;
	}

}


const john = new Customer('john@mail.com');
const prodA = new Product('soap', 9.99);




